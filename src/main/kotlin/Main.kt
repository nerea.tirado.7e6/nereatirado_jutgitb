import com.sun.scenario.effect.impl.prism.PrRenderInfo
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries

@Serializable
class Problema(
    val texto: String,
    val inputPublico: String,
    val outputPublico: Int,
    val inputPrivado: String,
    val outputPrivado: Int,
    var solucion: Boolean,
    val intentos: Int,
    val listaDeIntentos: String
)

fun mostrarMenuEstudiante() {
    println("Bienvenido a nuestro programa!!")
    println("1. Seguir amb l’itinerari d’aprenentatge")
    println("2. Llista problemes")
    println("3. Consultar històric de problemes resolts")
    println("4. Ayuda")
    println("0. Sortir")
    println("Selecciona una opción: ")
}

fun mostrarMenuProfessor() {
    println("Bienvenido a nuestro programa!!")
    println("1. Afegir nous problemas")
    println("2. Treure un report de la feina de l’alumne")
}

fun contraseñaProfessor() {
    var contraseña = "itb2023"
    var contraseñaUsuario = ""
    do {
        println("Escribe la contraseña")
        contraseñaUsuario = readln().toString()
    } while (contraseña != contraseñaUsuario)
}

fun numeroDeProblemas(ruta: Path): Int {
    if (ruta.exists()) {
        val files = ruta.listDirectoryEntries()
        return files.size
    }
    return 0
}

fun nuevoProblema(fichero: File, informacionProblema: String = "") {
    if (!fichero.exists()) {
        val correcto = fichero.createNewFile()
        if (correcto) {
            fichero.writeText(informacionProblema)
        }
    }
}

fun main() {
    var nomFitxer = ""
    println("Selecciona quien eres: ")
    println("1. Professor")
    println("2. Estudiante")
    var seleccionarQuienEres = readln().toInt()
    if (seleccionarQuienEres == 1) {
        contraseñaProfessor()
        do{
            mostrarMenuProfessor()
            var opcProfessor = readln().toInt()
            when (opcProfessor) {
                1-> {
                    var numeroProblemas = numeroDeProblemas(Path("./Problemas/"))
                    var ejercicio = numeroProblemas + 1
                    var nombreEjercicio = "Problemas/Ex" + ejercicio + ".json"
                    println("La opción seleccionada es: 1. Afegir nous problemas")
                    println("--------------------------------------------------------------------")
                    println("Introduce el enunciado: ")
                    var texto = readln()
                    println("Introduce el inputPublico: ")
                    var inputPublico = readln()
                    println("Introduce el outputPublico: ")
                    var outputPublico = readln().toInt()
                    println("Introduce el inputPrivado: ")
                    var inputPrivado = readln()
                    println("Introduce el outputPrivado: ")
                    var outputPrivado = readln().toInt()

                    var problemaGuardado =
                        "{\"texto\":\"" + texto + "\"" + ",\"inputPublico\":" + "\"" + inputPublico + "\"" + ",\"outputPublico\":" + outputPublico + ",\"inputPrivado\":" + "\"" + inputPrivado + "\"" + ",\"outputPrivado\":" + outputPrivado + "," + "\"solucion\": false," + "\"intentos\": 0," + "\"listaDeIntentos\": \"\"}"
                    println(problemaGuardado)
                    nuevoProblema(File(nombreEjercicio), problemaGuardado)
                }
                2-> {
                    do {
                        println("1.Treure una puntuació en funció dels problemes resolts")
                        println("2. Descomptar per intents")
                        println("3. Mostrar-ho de manera més o menys gràfica (a través de consola)")
                        println("Selecciona una opción de estas tres: ")
                        var selecciona = readln().toInt()
                        when (selecciona) {
                            1 -> {
                                var ejerciciosResueltos = 0
                                var ficheros = numeroDeProblemas(Path("./Problemas/"))
                                for (i in 1..ficheros) {
                                    val nomFitxer = "Problemas/Ex" + i + ".json"
                                    val file = File(nomFitxer)
                                    val fileData = file.readText()
                                    val json = Json.decodeFromString<Problema>(fileData)

                                    if (json.solucion == true) {
                                        ejerciciosResueltos++
                                    }
                                    println("Tienes $ejerciciosResueltos ejercicos resueltos")

                                }
                                var dividir = ejerciciosResueltos.toDouble() / ficheros.toDouble() * 100
                                println("EL porcentaje de problemas es: $dividir")
                            }
                            2-> {
                                var ejerciciosResuletos2 = 0
                                var sumar = 0
                                var solucionProblemas = 0
                                var ficheros = numeroDeProblemas(Path("./Problemas/"))
                                for (i in 1..ficheros) {
                                    val nomFitxer = "Problemas/Ex" + i + ".json"
                                    val file = File(nomFitxer)
                                    val fileData = file.readText()
                                    val json = Json.decodeFromString<Problema>(fileData)
                                    if (json.solucion==true){
                                        solucionProblemas++
                                    }
                                    ejerciciosResuletos2 = when (json.intentos) {
                                        1 -> 10
                                        2 -> 9
                                        3 -> 8
                                        4 -> 7
                                        5 -> 6
                                        6 -> 5
                                        7 -> 4
                                        8 -> 3
                                        9 -> 2
                                        10 -> 1
                                        else -> 0
                                    }
                                    sumar += ejerciciosResuletos2
                                }
                                var numeroResueltoproblema = sumar.toDouble()/solucionProblemas.toDouble()
                                println("La $sumar de la nota de los $solucionProblemas")
                            }
                            3-> {
                                var ejerciciosresueltos = 0
                                var ficheros = numeroDeProblemas(Path("./Problemas/"))
                                for (i in 1..ficheros) {
                                    val nomFitxer = "Problemas/Ex" + i + ".json"
                                    val file = File(nomFitxer)
                                    val fileData = file.readText()
                                    val json = Json.decodeFromString<Problema>(fileData)
                                    if (json.solucion==true) {
                                        println("\u001B[32m]$ejerciciosresueltos resuelto\u001B[0m]")
                                    } else {
                                        println("\u001B[31m]$ejerciciosresueltos no resuelto\u001B[0m]")
                                    }
                                }
                            }
                            0-> println("Has seleccionado salir del juego")
                            else-> println("Opción no vàlida")
                        }
                    } while (selecciona!=0)
                }
            }
        } while (opcProfessor != 0)
    } else if (seleccionarQuienEres == 2){
        do {
            mostrarMenuEstudiante()
            var opcSeleccionada = readln().toInt()
            when (opcSeleccionada) {
                1 -> {
                    println("La opción seleccionada es: 1. Seguir amb l’itinerari d’aprenentatge")
                    var ficheros = numeroDeProblemas(Path("./Problemas/"))
                    for (i in 1..ficheros) {
                        val nomFitxer = "Problemas/Ex" + i + ".json"
                        val file = File(nomFitxer)
                        val fileData = file.readText()
                        val json = Json.decodeFromString<Problema>(fileData)

                        if (json.solucion == false) {
                            println("Enunciado: ${json.texto}")
                            println("Entrada de prueba: ${json.inputPublico}")
                            println("Salida de prueba: ${json.outputPublico}")
                            println("¿Quieres solucionarlo? (S/N)")
                            val respSolucion = readln()
                            if (respSolucion == "S") {
                                do {
                                    var volver = "N"
                                    println("Entrada de datos: ${json.inputPrivado}")
                                    val solucionUsuario = readln().toInt()
                                    if (solucionUsuario == json.outputPrivado) {
                                        println("Felicidades!!")
                                        json.solucion = true
                                    } else {
                                        println("Error!!")
                                        println("¿Lo quieres volver intentar? (S/N)")
                                        volver = readln()
                                    }
                                } while (solucionUsuario != json.outputPrivado && volver == "S")
                                file.writeText(Json.encodeToString(json))
                            }
                        } else println("El problema $i esta solucionado")
                    }
                }

                2 -> {
                    println("La opción seleccionada es: 2. Llista problemas")
                    println("Estos son los 20 problemas que tiene e sistema: ")
                    var ficheros = numeroDeProblemas(Path("./Problemas/"))
                    for (i in 1..ficheros) {
                        val nomFitxer = "Problemas/Ex" + i + ".json"
                        val file = File(nomFitxer)
                        val fileData = file.readText()
                        val json = Json.decodeFromString<Problema>(fileData)
                        println(json.texto)
                    }
                    println("------------------------------------------------------------------------------------------------------------------------------")
                    println("Selecciona un problema")
                    var numeroUsuario = readln().toInt()
                    while (numeroUsuario < 1 || numeroUsuario > 20) {
                        println("ERROR NO ESTA ENTRE 1 Y 20")
                        numeroUsuario = readln().toInt()
                    }
                    nomFitxer = "Problemas/Ex" + numeroUsuario + ".json"
                    val file = File(nomFitxer)
                    val fileData = file.readText()
                    val json = Json.decodeFromString<Problema>(fileData)
                    println(json.texto)
                    do {
                        println("Aqui te mostramos un ejemplo: ")
                        println(json.inputPublico)
                        println(json.outputPublico)
                        println("* Ahora lo solucionaras *")
                        println(json.inputPrivado)
                        val respuestaUusuario = readln().toInt()
                        (json.outputPrivado)
                    } while (respuestaUusuario != json.outputPrivado)
                }

                3 -> {
                    println("La opción seleccionada es: 3. Consultar històric de problemas resolts")
                    var ficheros = numeroDeProblemas(Path("./Problemas/"))
                    for (i in 1..ficheros) {
                        val nomFitxer = "Problemas/Ex" + i + ".json"
                        val file = File(nomFitxer)
                        val fileData = file.readText()
                        val json = Json.decodeFromString<Problema>(fileData)
                        println("INTENTOS UTILIZADOS EN CADA PARTIDA:")
                        println("------------------------------------------------------")
                        println("Los intentos que utiliza en el problema ${i}: ${json.intentos}")
                        println("------------------------------------------------------")
                    }
                    println("HAN ESTADO SUPERADOS?")
                    var ficheros2 = numeroDeProblemas(Path("./Problemas/"))
                    for (i in 1..ficheros2) {
                        val nomFitxer = "Problemas/Ex" + i + ".json"
                        val file = File(nomFitxer)
                        val fileData = file.readText()
                        val json = Json.decodeFromString<Problema>(fileData)
                        if (json.solucion == true) {
                            println("El problema $i esta superado")
                        } else {
                            println("El problema $i no lo has superado")
                        }
                    }
                }

                4 -> {
                    println("La opción seleccionada es: 4. Ayuda")
                    println("La ayuda que te ofrecemos es la siguiente: ")
                    println("Este programa consiste de tener opción de seleccionar 5 opciones: ")
                    println("La primera sirve para poder mirar cuales han sido resueltos por el usuario i directamenmte que te mande al siguiente")
                    println("La segunda muestra la lista de problemas que tiene nuestro sistema a su misma vez te permite seleccionar uno en concreto")
                    println("La tercera muestra todos los intentos realizados en cada problema y si han estado superados o no")
                    println("La cuarta que en este caso es esta te ofrece una breve explicación del funcionamientos de cada apartado")
                    println("La quinta que en este caso es 0 te hace salir directamente del programa sin llegar a ser utilizado")
                }

                0 -> println("Has seleccionada que quieres salir del programa")
            }
        } while (opcSeleccionada != 0)

    }
}